from .base_menu import Menu
from ..models import User
from .tasks import make_payment

class request_gas(Menu):
    """Serves GEtting gas Callbacks"""
    
    def get_gas(self):
        try:
            Type = int(self.user_response)
        except ValueError as exc:
            return self.home()
        self.session["Type"] = int(self.user_response)
        self.session["level"] = 10
        menu_text = "Purchase gas type {} ?\n".format(self.session.get('Type'))
        menu_text += "1.Confirm\n2.Cancel"
        return self.ussd_proceed(menu_text)

    def confirm(self):
        Type = self.session.get(Type)
        if self.user_response == "1":
            menu_text = "We are sending you an Mpesa checkout for purchasing Gas at KES{} shortly".format(Type)
            make_payment.apply_async(
                kwargs={'phone_number': self.user.phone_number, 'amount': amount}
            )
        return self.ussd_end(menu_text)

        if self.user_response == "2":
            menu_text = "Thank you for doing business with us"
            return self.ussd_end(menu_text)

        return self.home()

    def execute(self):
        menu = {
            10: self.get_gas,
            11: self.confirm
        }
        return menu.get(self.level)()