from .base_menu import Menu
from .tasks import check_balance


class LowerLevelMenu(Menu):
    """serves the home menu"""
    def get_gas(self):
        menu_text = "Get gas\n" \
                    "1. Gas Meko @7k, 3k deposit + MI\n" \
                    "2. Gas Cylinder @12k, 5k deposit + MI\n" \
                    "0. Back"

        self.session["level"] = 10
        return self.ussd_proceed(menu_text)


    def check_payment(self):
        menu_text = "Please wait as we load your account\n You will recieve an SMS notification shortly"
        # send payment async
        check_payment.apply_async(kwargs={'user_id': self.user.id})
        self.session["level"] = 30
        return self.ussd_end(menu_text)


    def make_payment(self):
        menu_text = "To make payment via: \n"\
                    "1. Mpesa \n"\

        self.session['level']= 20
        return self.ussd_proceed(menu_text)

    def execute(self):
        menus = {
            '1': self.get_gas,
            '2': self.check_payment,
            '3': self.make_payment
           }
        return menus.get(self.user_response, self.home)()
